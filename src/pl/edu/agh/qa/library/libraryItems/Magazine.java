package pl.edu.agh.qa.library.libraryItems;

public class Magazine extends LibraryItem{

    private String magazineNumber;
    public String getMagazineNumber() {
        return magazineNumber;
    }


    public Magazine(String _title, String _magazineNumber, int _quantity) {
        super(_title, _quantity);
        magazineNumber = _magazineNumber;
    }

    public String Description() {
        return "Magazine: " + getTitle() + ";" + getMagazineNumber() + ";" + getQuantity();
    }

    public String ShortDescription() {
        return getTitle()+ "-"+getMagazineNumber();
    }
}
