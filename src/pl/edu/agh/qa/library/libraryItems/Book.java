package pl.edu.agh.qa.library.libraryItems;

public class Book extends LibraryItem {

    private String author;
    public String getAuthor() {
        return author;
    }


    public Book(String _title, String _author, int _quantity) {
        super(_title, _quantity);
        author = _author;
    }

    public String Description() {
        return "Book: " +getTitle() + ";" + getAuthor() + ";" + getQuantity();
    }

    @Override
    public String ShortDescription() {
        return getTitle()+ "-"+getAuthor();
    }
}
