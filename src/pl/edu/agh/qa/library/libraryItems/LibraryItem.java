package pl.edu.agh.qa.library.libraryItems;

import java.util.ArrayList;

public abstract class LibraryItem {
    private String title;
    private int quantity;
    private int uniqueId;

    private static int uniqueIdCounter = 1;

    ArrayList<Integer> borrowedByReaders;


    protected LibraryItem(String title, int quantity) {
        this.title = title;
        this.quantity = quantity;
        this.uniqueId = uniqueIdCounter++;
        borrowedByReaders = new ArrayList<>();
    }

    public int getQuantity() {
        return quantity;
    }

    public void addQuantity(int addedNumber)
    {
        quantity = quantity + addedNumber;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getTitle() {
        return title;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public abstract String Description();
    public abstract String ShortDescription();

    public boolean CanBorrow() {
        return NotBorrowedCount() > 0;
    }

    public int NotBorrowedCount()
    {
        return getQuantity() - borrowedByReaders.size();
    }

    public boolean Borrow(int readerId) {
        if (CanBorrow()) {
            borrowedByReaders.add(readerId);
            return true;
        }

        return false;
    }

    public boolean Return(int readerId) {
        if (borrowedByReaders.contains(readerId)) {
            borrowedByReaders.remove(readerId);
            return true;
        }
        return false;
    }

    public boolean IsBorrowedBy(int readerId) {
        if (borrowedByReaders.contains(readerId))
            return true;
        else
            return false;
    }
}




