package pl.edu.agh.qa.library;


import pl.edu.agh.qa.library.libraryItems.Book;
import pl.edu.agh.qa.library.libraryItems.LibraryItem;
import pl.edu.agh.qa.library.libraryItems.Magazine;
import pl.edu.agh.qa.library.users.Instructor;
import pl.edu.agh.qa.library.users.Reader;
import pl.edu.agh.qa.library.users.Student;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Library {
    private ArrayList<LibraryItem> items;
    private ArrayList<Reader> readers;

    public Library() {
        items = new ArrayList<>();
        readers = new ArrayList<>();
    }

    public ArrayList getReaders() {
        return readers;
    }

    public void AddBook(String title, String author, int quantity) {
        Book foundBook = FindBook(title, author);
        if(foundBook != null)
            foundBook.addQuantity(quantity);
        else
            items.add(new Book(title, author, quantity));
    }

    private Book FindBook(String title, String author) {
        for (LibraryItem item : items) {
            if (item instanceof Book){
                Book book = (Book)item;
                if (book.getTitle() == title && book.getAuthor() == author)
                    return book;
            }
        }
        return null;
    }

    public void AddMagazine(String title, String number, int quantity) {
        Magazine foundMagazine = FindMagazine(title, number);
        if (foundMagazine != null)
            foundMagazine.addQuantity(quantity);
        else
            items.add(new Magazine(title, number, quantity));
    }
    private Magazine FindMagazine(String title, String number) {
        for (LibraryItem item : items) {
            if (item instanceof Magazine){
                Magazine magazine = (Magazine)item;
                if (magazine.getTitle() == title && magazine.getMagazineNumber() == number)
                    return magazine;
            }
        }
        return null;
    }
    public void addStudent(String firstName, String lastName){
        readers.add(new Student(firstName, lastName));
    }

    public void addInstructor(String firstName, String lastName)
    {
        readers.add(new Instructor(firstName, lastName));
    }

    public boolean Borrow(int bookId, int readerId)
    {
        LibraryItem item = FindItemById(bookId);
        Reader reader = FindReaderById(readerId);
        if (item != null && reader != null && item.CanBorrow() )
        {
            ArrayList<LibraryItem> borrowedBooks = FindAllBorowedBooks(readerId);
            if (borrowedBooks.size() < reader.getMaximumNumberOfBooks() ) {
                item.Borrow(readerId);
                return true;
            }
        }
        return false;
    }

    public ArrayList<LibraryItem> FindAllBorowedBooks(int readerId)
    {
        ArrayList<LibraryItem> borrowedBooks = new ArrayList<>();

        for (LibraryItem item : items) {
            if (item.IsBorrowedBy(readerId))
                borrowedBooks.add(item);
        }

        return borrowedBooks;
    }

    private Reader FindReaderById(int readerId) {
        for (Reader reader : readers){
            if (reader.getId() == readerId)
                return reader;
        }
        return null;
    }

    private LibraryItem FindItemById(int id) {
        for (LibraryItem item : items) {
            if (item.getUniqueId() == id)
                return item;
        }
        return null;

    }

    public void PrintAllLibraryItems() {
        for (LibraryItem item : items) {
            System.out.println(item.Description());
        }

    }
    public  void  PrintAllReaders(){
        for (Reader readers : readers){
            System.out.println(readers.Description());
        }
    }


    public void writeUseresWithBorrowedBooks(String filePath) {
        PrintWriter writer = null;
        try {
            //create a temporary file
            writer = new PrintWriter(filePath);

            for (Reader reader : readers){
                ArrayList<LibraryItem> borrowed = FindAllBorowedBooks(reader.getId());
                if (borrowed.size() >= 1)
                {
                    writer.println( formatReader(reader, borrowed));
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String formatReader(Reader reader, ArrayList<LibraryItem> borrowed) {
        String description = reader.getFirstName() + " " + reader.getLastName() + " [";
        for (LibraryItem item: borrowed) {
            description += item.ShortDescription()+";";
        }
        description += "]";
        return description;
    }

    public void loadFromFile(String filePath) {
        List<String> list = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            list = stream.collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String line: list) {
            String[] parts = line.split(";");
            try {
                if (parts.length == 3) {
                    AddBook(parts[0].trim(), parts[1].trim(), Integer.parseInt(parts[2].trim()));
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }
}



