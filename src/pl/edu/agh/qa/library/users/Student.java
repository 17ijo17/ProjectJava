package pl.edu.agh.qa.library.users;

public class Student extends Reader {
    private static final int maxBooks = 4;

    public Student(String firstName, String lastName) {
        super(firstName, lastName, maxBooks);
    }

    @Override
    public String Status() {
        return "S";
    }


}
