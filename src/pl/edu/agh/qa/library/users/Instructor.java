package pl.edu.agh.qa.library.users;

import java.util.ArrayList;

public class Instructor extends Reader {
    private static final int maxBooks = 10;

    public Instructor(String firstName, String lastName) {
        super(firstName, lastName, maxBooks);
    }

    @Override
    public String Status() {
        return "W";
    }


}

