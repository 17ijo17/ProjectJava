package pl.edu.agh.qa.library.users;

import java.util.ArrayList;
import java.util.Objects;

public abstract class Reader {
    private int id;
    private String firstName;
    private String lastName;
    private int maximumNumberOfBooks;

    private static int lastId = 1;


    protected Reader(String firstName, String lastName, int maximumNumberOfBooks) {
        this.id = getNextId();
        this.firstName = firstName;
        this.lastName = lastName;
        this.maximumNumberOfBooks = maximumNumberOfBooks;
    }

    private int getNextId() {
        return lastId++;
    }


    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {

        return lastName;
    }

    public int getMaximumNumberOfBooks() {
        return maximumNumberOfBooks;
    }


    public  String Description(){
        return  getFirstName() + ";" + getLastName() + ";"+ getId()+";" + Status();
    };

    public abstract String Status();

}
