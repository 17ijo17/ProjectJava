package pl.edu.agh.qa.library;

public class Starter {

    public static void main(String[] args) {

        Library library = new Library();

        library.addInstructor("Jaś", "Fasola");
        library.addStudent("Antek", "Kania");
        library.addInstructor("Anna", "Fasola");
        library.PrintAllReaders();

        library.AddBook("Zemsta", "Fredro", 12);
        library.AddMagazine("Młody Technik", "01/2005", 5);
        library.AddBook("Gra", "Opioła", 3)   ;
        library.AddMagazine("Murator", "22-02-15", 3);
        library.AddBook("Ula", "Janusz", 12);
        library.AddMagazine("Focus", "12/2006", 5);
        library.AddMagazine("Wiedza i Życie", "2012/01/12", 15);
        library.AddBook("Zemsta Nietoperza", "Ja", 33);
        library.loadFromFile("e:/loadLibrary.txt");

        library.Borrow(1, 1);
        library.Borrow(3, 1);
        library.Borrow(3, 2);
        library.Borrow(4, 2);
        library.Borrow(5, 2);


        library.writeUseresWithBorrowedBooks("e:/library.txt");
        library.PrintAllLibraryItems();

    }







}
